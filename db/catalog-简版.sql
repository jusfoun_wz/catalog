/*
Navicat Oracle Data Transfer
Oracle Client Version : 11.2.0.1.0

Source Server         : ora
Source Server Version : 110200
Source Host           : 61.155.214.204:1521
Source Schema         : ORACLE

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2016-03-30 16:04:35
*/


-- ----------------------------
-- Table structure for BUSINESS
-- ----------------------------
DROP TABLE "ORACLE"."BUSINESS";
CREATE TABLE "ORACLE"."BUSINESS" (
"ID" NUMBER(10) NOT NULL ,
"NAME" NVARCHAR2(255) NOT NULL ,
"NAME_REF" NVARCHAR2(255) NULL ,
"FIRST_NAME" NVARCHAR2(255) NULL ,
"SECOND_NAME" NVARCHAR2(255) NULL ,
"THIRD_NAME" NVARCHAR2(255) NULL ,
"CODE" NVARCHAR2(255) NULL ,
"TYPE" NVARCHAR2(255) NULL ,
"BASIS" NVARCHAR2(255) NULL ,
"FLOW" NVARCHAR2(255) NULL ,
"SUMMARY" NVARCHAR2(255) NULL ,
"TIME_LIMIT" NVARCHAR2(255) NULL ,
"CHARGE_BASIS" NVARCHAR2(255) NULL ,
"CHARGE_OFFICE_ID" NUMBER(10) NULL ,
"IMPL_OFFICE_ID" NUMBER(10) NULL ,
"WORKLOAD" NVARCHAR2(255) NULL ,
"RELATE_OFFICE" NVARCHAR2(255) NULL ,
"NEED_DATA" NVARCHAR2(255) NULL ,
"PRODUCE_DATA" NVARCHAR2(255) NULL ,
"IS_USE" NVARCHAR2(255) NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"SHARE_WITH" NVARCHAR2(255) NULL ,
"CREATE_BY" NUMBER(10) NULL ,
"CREATE_DATE" TIMESTAMP(4)  NULL ,
"UPDATE_BY" NUMBER(10) NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NULL ,
"DEL_FLAG" NVARCHAR2(255) NOT NULL ,
"STATUS" NVARCHAR2(255) DEFAULT 0  NULL ,
"SUBJECT_ID" NUMBER(10) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;


-- ----------------------------
-- Table structure for BUSINESS_RESOURCE
-- ----------------------------
DROP TABLE "ORACLE"."BUSINESS_RESOURCE";
CREATE TABLE "ORACLE"."BUSINESS_RESOURCE" (
"BUSINESS_ID" NUMBER(10) NULL ,
"RESOURCE_ID" NUMBER(10) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of BUSINESS_RESOURCE
-- ----------------------------

-- ----------------------------
-- Table structure for CATALOG_INFO
-- ----------------------------
DROP TABLE "ORACLE"."CATALOG_INFO";
CREATE TABLE "ORACLE"."CATALOG_INFO" (
"ID" NUMBER(10) NOT NULL ,
"TYPE" NUMBER(10) NULL ,
"TYPE_VALUE" NVARCHAR2(255) NULL ,
"OFFICE_ID" NUMBER(10) NULL ,
"CREATE_DATE" TIMESTAMP(4)  NULL ,
"CREATE_BY" NUMBER(10) NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NULL ,
"UPDATE_BY" NUMBER(10) NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Table structure for JOB
-- ----------------------------
DROP TABLE "ORACLE"."JOB";
CREATE TABLE "ORACLE"."JOB" (
"ID" NUMBER(10) NOT NULL ,
"NAME" NVARCHAR2(255) NOT NULL ,
"DUTY" NVARCHAR2(255) NULL ,
"TYPE" NVARCHAR2(255) NULL ,
"CREATE_BY" NUMBER(10) NOT NULL ,
"CREATE_DATE" TIMESTAMP(4)  NOT NULL ,
"UPDATE_BY" NUMBER(10) NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NOT NULL ,
"STATUS" NVARCHAR2(255) NULL ,
"OFFICE_ID" NUMBER(10) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Table structure for JOB_BUSINESS
-- ----------------------------
DROP TABLE "ORACLE"."JOB_BUSINESS";
CREATE TABLE "ORACLE"."JOB_BUSINESS" (
"JOB_ID" NUMBER(20) NOT NULL ,
"BUSINESS_ID" NUMBER(20) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;


-- ----------------------------
-- Table structure for REGISTER
-- ----------------------------
DROP TABLE "ORACLE"."REGISTER";
CREATE TABLE "ORACLE"."REGISTER" (
"ID" NUMBER(10) NOT NULL ,
"OFFICE_ID" NUMBER(20) NOT NULL ,
"APPLY_TYPE" VARCHAR2(255 BYTE) NULL ,
"APPLY_ID" NUMBER(9) NULL ,
"APPLY_NAME" NVARCHAR2(255) NULL ,
"APPLY_BY" NUMBER(20) NULL ,
"APPLY_DATE" TIMESTAMP(4)  NULL ,
"APPROVE_BY" NUMBER(20) NULL ,
"APPROVE_DATE" TIMESTAMP(4)  NULL ,
"APPLY_FLAG" NVARCHAR2(255) NOT NULL ,
"APPROVE_FLAG" NVARCHAR2(255) NOT NULL ,
"DEL_FLAG" NVARCHAR2(255) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Table structure for RESOURCE_INFO
-- ----------------------------
DROP TABLE "ORACLE"."RESOURCE_INFO";
CREATE TABLE "ORACLE"."RESOURCE_INFO" (
"ID" NUMBER(10) NOT NULL ,
"NAME" NVARCHAR2(255) NULL ,
"OF_BUSINESS" NVARCHAR2(255) NULL ,
"TYPE" NUMBER(10) NULL ,
"CODE" NVARCHAR2(255) NULL ,
"DESCR" NVARCHAR2(255) NULL ,
"RESPONSE_PARTY" NVARCHAR2(255) NULL ,
"RESPONSE_ATTR" NUMBER(10) NULL ,
"RESPONSE_CU" NVARCHAR2(255) NULL ,
"COLLECT_WAY" NUMBER(10) NULL ,
"SECURITY_LEVEL" NUMBER(10) NULL ,
"INFO_FIELD" NVARCHAR2(255) NULL ,
"IS_DB_SUPPORT" NUMBER(10) NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"SHARE_REGION" NVARCHAR2(255) NULL ,
"SHARE_MODE" NVARCHAR2(255) NULL ,
"CREATE_DATE" TIMESTAMP(4)  NULL ,
"CREATE_BY" NUMBER(10) NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NULL ,
"UPDATE_BY" NUMBER(10) NULL ,
"UPDATE_CYCLE" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NULL ,
"STATUS" NVARCHAR2(255) DEFAULT 0  NULL ,
"SUBJECT_ID" NUMBER(10) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Table structure for RPT_SEARCH
-- ----------------------------
DROP TABLE "ORACLE"."RPT_SEARCH";
CREATE TABLE "ORACLE"."RPT_SEARCH" (
"ID" NUMBER(10) NOT NULL ,
"KEYWORD" NVARCHAR2(255) NULL ,
"SRHTIME" TIMESTAMP(4)  NULL ,
"SRHTYPE" NUMBER(10) NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"OFFICEID" NUMBER NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;
COMMENT ON COLUMN "ORACLE"."RPT_SEARCH"."ID" IS '主键ID';
COMMENT ON COLUMN "ORACLE"."RPT_SEARCH"."KEYWORD" IS '搜索关键字';
COMMENT ON COLUMN "ORACLE"."RPT_SEARCH"."SRHTIME" IS '搜索时间';
COMMENT ON COLUMN "ORACLE"."RPT_SEARCH"."SRHTYPE" IS '搜索类型（1：机构，2：岗位，3：业务，4：资源，5：主题）';
COMMENT ON COLUMN "ORACLE"."RPT_SEARCH"."REMARKS" IS '备注';
COMMENT ON COLUMN "ORACLE"."RPT_SEARCH"."OFFICEID" IS '搜索的机构Id';

-- ----------------------------
-- Table structure for SUBJECT_INFO
-- ----------------------------
DROP TABLE "ORACLE"."SUBJECT_INFO";
CREATE TABLE "ORACLE"."SUBJECT_INFO" (
"ID" NUMBER(10) NOT NULL ,
"NAME" NVARCHAR2(500) NOT NULL ,
"PARENT_ID" NUMBER(10) NULL ,
"PARENT_NAME" NVARCHAR2(500) NULL ,
"DESCR" NVARCHAR2(1000) NULL ,
"SHARE_REGION" NVARCHAR2(500) NULL ,
"SHARE_MODE" NVARCHAR2(500) NULL ,
"STATUS" NUMBER(10) DEFAULT 0  NULL ,
"CREATE_DATE" TIMESTAMP(4)  NULL ,
"CREATE_BY" NUMBER(10) NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NULL ,
"UPDATE_BY" NUMBER(10) NULL ,
"REMARKS" NVARCHAR2(1000) NULL ,
"DEL_FLAG" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;
-- ----------------------------
-- Table structure for SYS_DICT
-- ----------------------------
DROP TABLE "ORACLE"."SYS_DICT";
CREATE TABLE "ORACLE"."SYS_DICT" (
"ID" NUMBER(10) NOT NULL ,
"VALUE" NVARCHAR2(255) NOT NULL ,
"LABEL" NVARCHAR2(255) NOT NULL ,
"TYPE" NVARCHAR2(255) NULL ,
"DESCRIPTION" NVARCHAR2(255) NULL ,
"SORT" NUMBER(19,5) NULL ,
"PARENT_ID" NUMBER(10) NULL ,
"CREATE_BY" NUMBER(10) NULL ,
"CREATE_DATE" TIMESTAMP(4)  NULL ,
"UPDATE_BY" NUMBER(10) NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Table structure for SYS_LOG
-- ----------------------------
DROP TABLE "ORACLE"."SYS_LOG";
CREATE TABLE "ORACLE"."SYS_LOG" (
"ID" NUMBER(10) NOT NULL ,
"TYPE" NVARCHAR2(255) NULL ,
"TITLE" NVARCHAR2(255) NULL ,
"CREATE_BY" NUMBER(10) NULL ,
"CREATE_DATE" TIMESTAMP(4)  NULL ,
"REMOTE_ADDR" NVARCHAR2(255) NULL ,
"USER_AGENT" NVARCHAR2(255) NULL ,
"REQUEST_URI" NVARCHAR2(255) NULL ,
"METHOD" NVARCHAR2(255) NULL ,
"PARAMS" NVARCHAR2(1000) NULL ,
"EXCEPTION" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Table structure for SYS_MDICT
-- ----------------------------
DROP TABLE "ORACLE"."SYS_MDICT";
CREATE TABLE "ORACLE"."SYS_MDICT" (
"ID" NUMBER(10) NOT NULL ,
"PARENT_ID" NUMBER(10) NOT NULL ,
"PARENT_IDS" NVARCHAR2(255) NOT NULL ,
"NAME" NVARCHAR2(255) NOT NULL ,
"SORT" NUMBER(19,5) NOT NULL ,
"DESCRIPTION" NVARCHAR2(255) NULL ,
"CREATE_BY" NUMBER(10) NOT NULL ,
"CREATE_DATE" TIMESTAMP(4)  NOT NULL ,
"UPDATE_BY" NUMBER(10) NOT NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NOT NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_MDICT
-- ----------------------------

-- ----------------------------
-- Table structure for SYS_MENU
-- ----------------------------
DROP TABLE "ORACLE"."SYS_MENU";
CREATE TABLE "ORACLE"."SYS_MENU" (
"ID" NUMBER(10) NOT NULL ,
"PARENT_ID" NUMBER(10) NOT NULL ,
"PARENT_IDS" NVARCHAR2(255) NOT NULL ,
"NAME" NVARCHAR2(255) NOT NULL ,
"SORT" NUMBER(19,5) NOT NULL ,
"HREF" NVARCHAR2(255) NULL ,
"TARGET" NVARCHAR2(255) NULL ,
"ICON" NVARCHAR2(255) NULL ,
"IS_SHOW" NVARCHAR2(255) NOT NULL ,
"PERMISSION" NVARCHAR2(255) NULL ,
"CREATE_BY" NUMBER(10) NOT NULL ,
"CREATE_DATE" TIMESTAMP(4)  NOT NULL ,
"UPDATE_BY" NUMBER(10) NOT NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NOT NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_MENU
-- ----------------------------
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('1', '0', '0,', '功能菜单', '0', null, null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 09:05:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 09:05:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('2', '1', '0,1,', '系统管理', '900', null, null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 09:05:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 09:05:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('3', '2', '0,1,2,', '信息统计', '980', '/admin/statistic/info', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 09:05:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 09:05:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('4', '2', '0,1,2,', '检索统计', '10', '/admin/statistic/search', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('5', '2', '0,1,2,', '使用日志', '20', '/admin/log/list', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('6', '2', '0,1,2,', '字典管理', '30', '/admin/dict/list', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('7', '1', '0,1,', '共享目录', '100', null, null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('8', '7', '0,1,7,', '机构职责信息', '10', '/admin/office/info', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 14:27:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('9', '7', '0,1,7,', '部门岗位信息', '20', '/admin/job/info', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-23 14:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-23 14:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('10', '1', '0,1,', '目录管理', '400', null, null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('11', '10', '0,1,10,', '机构目录管理', '10', '/admin/office/manage', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('12', '10', '0,1,10,', '岗位目录管理', '20', '/admin/job/manage', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('13', '10', '0,1,10,', '业务目录管理', '30', '/admin/business/manage', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('14', '10', '0,1,10,', '资源目录管理', '40', '/admin/resource/manage', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('15', '10', '0,1,10,', '主题目录管理', '50', '/admin/subject/manage', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-24 14:24:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('17', '7', '0,1,7,', '业务目录信息', '30', '/admin/business/index', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('18', '7', '0,1,7,', '资源目录信息', '40', '/admin/resource/index', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('19', '7', '0,1,7,', '主题目录信息', '50', '/admin/subject/index', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('20', '1', '0,1,', '部门目录', '200', null, null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('21', '20', '0,1,20,', '机构职责维护', '10', '/admin/office/maintenance', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('22', '20', '0,1,20,', '部门岗位维护', '20', '/admin/job/maintenance', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:21:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('24', '20', '0,1,20,', '业务信息维护', '40', '/admin/business/maintenance', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('26', '20', '0,1,20,', '资源信息维护', '60', '/admin/resource/maintenance', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('28', '20', '0,1,20,', '主题信息关联', '80', '/admin/subject/link', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('29', '1', '0,1,', '注册申请', '300', null, null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '1');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('30', '1', '0,1,', '注册申请', '300', '/admin/register/applyIndex', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:31:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('31', '1', '0,1,', '注册审批', '500', null, null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:57:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:57:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '1');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('32', '1', '0,1,', '注册审批', '500', '/admin/register/approveIndex', null, null, '1', null, '1', TO_TIMESTAMP(' 2015-12-29 17:57:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-29 17:57:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_MENU" VALUES ('33', '2', '0,1,2,', '人员管理', '40', '/admin/user/list', null, null, '1', null, '1', TO_TIMESTAMP(' 2016-03-01 10:38:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2016-03-01 10:38:34:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');

-- ----------------------------
-- Table structure for SYS_OFFICE
-- ----------------------------
DROP TABLE "ORACLE"."SYS_OFFICE";
CREATE TABLE "ORACLE"."SYS_OFFICE" (
"ID" NUMBER(9) NOT NULL ,
"PARENT_ID" NUMBER(9) NOT NULL ,
"PARENT_IDS" NVARCHAR2(255) NULL ,
"NAME" NVARCHAR2(255) NOT NULL ,
"SORT" NUMBER(9) NULL ,
"AREA_ID" NVARCHAR2(255) NULL ,
"CODE" NVARCHAR2(255) NULL ,
"TYPE" NVARCHAR2(255) NULL ,
"GRADE" NVARCHAR2(255) NULL ,
"ADDRESS" NVARCHAR2(255) NULL ,
"ZIP_CODE" NVARCHAR2(255) NULL ,
"MASTER" NVARCHAR2(255) NULL ,
"PHONE" NVARCHAR2(255) NULL ,
"FAX" NVARCHAR2(255) NULL ,
"EMAIL" NVARCHAR2(255) NULL ,
"USEABLE" NVARCHAR2(255) NULL ,
"PRIMARY_PERSON" NUMBER(9) NULL ,
"DEPUTY_PERSON" NUMBER(9) NULL ,
"CREATE_BY" NUMBER(9) NULL ,
"CREATE_DATE" TIMESTAMP(4)  NULL ,
"UPDATE_BY" NUMBER(9) NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NULL ,
"DUTY" NVARCHAR2(255) NULL ,
"STATUS" NVARCHAR2(255) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Table structure for SYS_ROLE
-- ----------------------------
DROP TABLE "ORACLE"."SYS_ROLE";
CREATE TABLE "ORACLE"."SYS_ROLE" (
"ID" NUMBER(10) NOT NULL ,
"OFFICE_ID" NUMBER(10) NULL ,
"NAME" NVARCHAR2(255) NOT NULL ,
"ENNAME" NVARCHAR2(255) NULL ,
"ROLE_TYPE" NVARCHAR2(255) NULL ,
"DATA_SCOPE" NVARCHAR2(255) NULL ,
"IS_SYS" NVARCHAR2(255) NULL ,
"USEABLE" NVARCHAR2(255) NULL ,
"CREATE_BY" NUMBER(10) NOT NULL ,
"CREATE_DATE" TIMESTAMP(4)  NOT NULL ,
"UPDATE_BY" NUMBER(10) NOT NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NOT NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_ROLE
-- ----------------------------
INSERT INTO "ORACLE"."SYS_ROLE" VALUES ('1', '1', '系统管理员', 'dept', 'assignment', '1', null, '1', '1', TO_TIMESTAMP(' 2015-12-22 21:17:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-22 21:17:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');
INSERT INTO "ORACLE"."SYS_ROLE" VALUES ('4', '1', '部门管理员', 'b', 'assignment', '4', null, '1', '1', TO_TIMESTAMP(' 2015-12-22 21:17:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-22 21:17:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');

-- ----------------------------
-- Table structure for SYS_ROLE_MENU
-- ----------------------------
DROP TABLE "ORACLE"."SYS_ROLE_MENU";
CREATE TABLE "ORACLE"."SYS_ROLE_MENU" (
"ROLE_ID" NUMBER(10) NOT NULL ,
"MENU_ID" NUMBER(10) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_ROLE_MENU
-- ----------------------------
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '1');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '2');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '3');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '4');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '5');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '6');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '7');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '8');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '9');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '10');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '11');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '12');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '13');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '14');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '15');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '17');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '18');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '19');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '31');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '32');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '1');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '7');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '8');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '9');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '17');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '18');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '19');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '20');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '21');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '22');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '24');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '26');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '28');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '29');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('4', '30');
INSERT INTO "ORACLE"."SYS_ROLE_MENU" VALUES ('1', '33');

-- ----------------------------
-- Table structure for SYS_ROLE_OFFICE
-- ----------------------------
DROP TABLE "ORACLE"."SYS_ROLE_OFFICE";
CREATE TABLE "ORACLE"."SYS_ROLE_OFFICE" (
"ROLE_ID" NUMBER(10) NOT NULL ,
"OFFICE_ID" NUMBER(10) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_ROLE_OFFICE
-- ----------------------------

-- ----------------------------
-- Table structure for SYS_USER
-- ----------------------------
DROP TABLE "ORACLE"."SYS_USER";
CREATE TABLE "ORACLE"."SYS_USER" (
"ID" NUMBER(9) NOT NULL ,
"COMPANY_ID" NUMBER(9) NULL ,
"OFFICE_ID" NUMBER(9) NOT NULL ,
"LOGIN_NAME" NVARCHAR2(255) NOT NULL ,
"PASSWORD" NVARCHAR2(255) NOT NULL ,
"NO" NVARCHAR2(255) NULL ,
"NAME" NVARCHAR2(255) NOT NULL ,
"EMAIL" NVARCHAR2(255) NULL ,
"PHONE" NVARCHAR2(255) NULL ,
"MOBILE" NVARCHAR2(255) NULL ,
"USER_TYPE" NVARCHAR2(255) NULL ,
"PHOTO" NVARCHAR2(255) NULL ,
"LOGIN_IP" NVARCHAR2(255) NULL ,
"LOGIN_DATE" TIMESTAMP(4)  NULL ,
"LOGIN_FLAG" NVARCHAR2(255) NULL ,
"CREATE_BY" NUMBER(9) NOT NULL ,
"CREATE_DATE" TIMESTAMP(4)  NULL ,
"UPDATE_BY" NUMBER(9) NULL ,
"UPDATE_DATE" TIMESTAMP(4)  NULL ,
"REMARKS" NVARCHAR2(255) NULL ,
"DEL_FLAG" NVARCHAR2(255) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_USER
-- ----------------------------
INSERT INTO "ORACLE"."SYS_USER" VALUES ('1', '135', '135', 'admin', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0001', '系统管理员', '28528000@qq.com', '8675', '8675', null, null, '202.106.10.250', TO_TIMESTAMP(' 2016-03-15 17:44:38:1660', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', '1', TO_TIMESTAMP(' 2015-12-21 21:12:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-21 21:12:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '最高管理员', '0');
INSERT INTO "ORACLE"."SYS_USER" VALUES ('2', '135', '135', 'sd_admin', 'c65f9d60e7593b2efa10e2eac7d50067e9ce6d1e60d48095fb1f1b85', '0002', '部门管理员', null, null, null, null, null, '202.106.10.250', TO_TIMESTAMP(' 2016-03-09 11:35:46:9180', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', '1', TO_TIMESTAMP(' 2015-12-21 21:12:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), '1', TO_TIMESTAMP(' 2015-12-21 21:12:00:0000', 'YYYY-MM-DD HH24:MI:SS:FF4'), null, '0');

-- ----------------------------
-- Table structure for SYS_USER_ROLE
-- ----------------------------
DROP TABLE "ORACLE"."SYS_USER_ROLE";
CREATE TABLE "ORACLE"."SYS_USER_ROLE" (
"USER_ID" NUMBER(10) NOT NULL ,
"ROLE_ID" NUMBER(10) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_USER_ROLE
-- ----------------------------
INSERT INTO "ORACLE"."SYS_USER_ROLE" VALUES ('1', '1');
INSERT INTO "ORACLE"."SYS_USER_ROLE" VALUES ('2', '4');


-- ----------------------------
-- View structure for INTEGRATION_STIC_VW
-- ----------------------------
CREATE OR REPLACE FORCE VIEW "ORACLE"."INTEGRATION_STIC_VW" AS 
select t.stime,t.sCount,t.type from (
SELECT
    to_char(o.create_date,'yyyy-mm') AS stime,count(o.id) as sCount ,'1' as type
FROM
  sys_office  o
GROUP BY
  to_char(o.create_date,'yyyy-mm')
union
SELECT
    to_char(j.create_date,'yyyy-mm') AS stime,count(j.id) as sCount ,'2' as type
FROM
  job  j
GROUP BY
  to_char(j.create_date,'yyyy-mm')
union
SELECT
    to_char(b.create_date,'yyyy-mm') AS stime,count(b.id) as sCount ,'3' as type
FROM
  business  b
GROUP BY
  to_char(b.create_date,'yyyy-mm')
union
SELECT
    to_char(r.create_date,'yyyy-mm') AS stime,count(r.id) as sCount ,'4' as type
FROM
  resource_info  r
GROUP BY
  to_char(r.create_date,'yyyy-mm')
union
SELECT
    to_char(s.create_date,'yyyy-mm') AS stime,count(s.id) as sCount ,'5' as type
FROM
  subject_info  s
GROUP BY
  to_char(s.create_date,'yyyy-mm')
) t order by t.type,stime;

-- ----------------------------
-- View structure for OFFICE_STIC_VW
-- ----------------------------
CREATE OR REPLACE FORCE VIEW "ORACLE"."OFFICE_STIC_VW" AS 
select
  d.id,e.demain,d.name as officeName ,d.duty,d.status
from
  SYS_OFFICE d ,(select c.id, c.firstDomain || '/' || c.secondDomain as demain from (select
        o.id,o.parent_id,o.name as secondDomain,s.name as firstDomain
    from
      SYS_OFFICE o,(select t.name, t.id from SYS_OFFICE t where t.parent_id = 0 and t.del_flag=0) s
    where
        o.parent_id = s.id
    ) c
  ) e
where e.id=d.parent_id
union
select h.id,h.demain,h.firstOffice||'/'||h.secondOffice as officeName,h.duty,h.status from (
 select f.id,f.name as secondOffice,f.parent_id,g.firstOffice,g.demain,f.duty,f.status from  SYS_OFFICE f,
  (select d.id,d.name as firstOffice,d.parent_id,e.demain from SYS_OFFICE d ,
  (select c.id, c.firstDomain || '/' || c.secondDomain as demain
  from (select o.id,o.parent_id,o.name as secondDomain,s.name as firstDomain
   from SYS_OFFICE o,(select t.name, t.id from SYS_OFFICE t where t.parent_id = 0 and t.del_flag=0) s
   where o.parent_id = s.id
  ) c
  ) e
  where e.id=d.parent_id)g
  where g.id=f.parent_id
 )h;

-- ----------------------------
-- Sequence structure for CATALOG_SEQ
-- ----------------------------
DROP SEQUENCE "ORACLE"."CATALOG_SEQ";
CREATE SEQUENCE "ORACLE"."CATALOG_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999999999999999999999
 START WITH 6983
 CACHE 20;

-- ----------------------------
-- Sequence structure for RESOURCE_INFO_SEQ
-- ----------------------------
DROP SEQUENCE "ORACLE"."RESOURCE_INFO_SEQ";
CREATE SEQUENCE "ORACLE"."RESOURCE_INFO_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999999999999999999999
 START WITH 101
 CACHE 20;

-- ----------------------------
-- Sequence structure for SYS_OFFICE_SEQ
-- ----------------------------
DROP SEQUENCE "ORACLE"."SYS_OFFICE_SEQ";
CREATE SEQUENCE "ORACLE"."SYS_OFFICE_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999999999999999999999
 START WITH 214
 CACHE 20;

-- ----------------------------
-- Sequence structure for SYS_USER_SEQ
-- ----------------------------
DROP SEQUENCE "ORACLE"."SYS_USER_SEQ";
CREATE SEQUENCE "ORACLE"."SYS_USER_SEQ"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999999999999999999999999999
 START WITH 101
 CACHE 20;

-- ----------------------------
-- Indexes structure for table BUSINESS
-- ----------------------------

-- ----------------------------
-- Triggers structure for table BUSINESS
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."BUSINESS_TRI" BEFORE INSERT ON "ORACLE"."BUSINESS" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;
-- ----------------------------
-- Checks structure for table BUSINESS
-- ----------------------------
ALTER TABLE "ORACLE"."BUSINESS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."BUSINESS" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."BUSINESS" ADD CHECK ("DEL_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."BUSINESS" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."BUSINESS" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."BUSINESS" ADD CHECK ("DEL_FLAG" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table BUSINESS
-- ----------------------------
ALTER TABLE "ORACLE"."BUSINESS" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table CATALOG_INFO
-- ----------------------------

-- ----------------------------
-- Triggers structure for table CATALOG_INFO
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."CATALOG_INFO_TRI" BEFORE INSERT ON "ORACLE"."CATALOG_INFO" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;

-- ----------------------------
-- Checks structure for table CATALOG_INFO
-- ----------------------------
ALTER TABLE "ORACLE"."CATALOG_INFO" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."CATALOG_INFO" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table CATALOG_INFO
-- ----------------------------
ALTER TABLE "ORACLE"."CATALOG_INFO" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table JOB
-- ----------------------------

-- ----------------------------
-- Triggers structure for table JOB
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."JOB_TRI" BEFORE INSERT ON "ORACLE"."JOB" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;
-- ----------------------------
-- Checks structure for table JOB
-- ----------------------------
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("DEL_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("DUTY" IS NOT NULL) DISABLE;
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB" ADD CHECK ("DEL_FLAG" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table JOB
-- ----------------------------
ALTER TABLE "ORACLE"."JOB" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table JOB_BUSINESS
-- ----------------------------
ALTER TABLE "ORACLE"."JOB_BUSINESS" ADD CHECK ("JOB_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB_BUSINESS" ADD CHECK ("BUSINESS_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB_BUSINESS" ADD CHECK ("JOB_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."JOB_BUSINESS" ADD CHECK ("BUSINESS_ID" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table REGISTER
-- ----------------------------

-- ----------------------------
-- Triggers structure for table REGISTER
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."REGISTER_TRI" BEFORE INSERT ON "ORACLE"."REGISTER" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;

-- ----------------------------
-- Checks structure for table REGISTER
-- ----------------------------
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("OFFICE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("APPLY_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("APPROVE_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("DEL_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("OFFICE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("APPLY_TYPE" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("APPLY_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("APPROVE_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."REGISTER" ADD CHECK ("DEL_FLAG" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table REGISTER
-- ----------------------------
ALTER TABLE "ORACLE"."REGISTER" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table RESOURCE_INFO
-- ----------------------------

-- ----------------------------
-- Triggers structure for table RESOURCE_INFO
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."RESOURCE_INFO_TRI" BEFORE INSERT ON "ORACLE"."RESOURCE_INFO" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT resource_info_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;
-- ----------------------------
-- Checks structure for table RESOURCE_INFO
-- ----------------------------
ALTER TABLE "ORACLE"."RESOURCE_INFO" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."RESOURCE_INFO" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table RESOURCE_INFO
-- ----------------------------
ALTER TABLE "ORACLE"."RESOURCE_INFO" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table RPT_SEARCH
-- ----------------------------

-- ----------------------------
-- Triggers structure for table RPT_SEARCH
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"." RPT_SEARCH_TRI" BEFORE INSERT ON "ORACLE"."RPT_SEARCH" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;
-- ----------------------------
-- Checks structure for table RPT_SEARCH
-- ----------------------------
ALTER TABLE "ORACLE"."RPT_SEARCH" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."RPT_SEARCH" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."RPT_SEARCH" ADD CHECK ("KEYWORD" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table RPT_SEARCH
-- ----------------------------
ALTER TABLE "ORACLE"."RPT_SEARCH" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table SUBJECT_INFO
-- ----------------------------

-- ----------------------------
-- Triggers structure for table SUBJECT_INFO
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."SUBJECT_INFO_TRI" BEFORE INSERT ON "ORACLE"."SUBJECT_INFO" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;
-- ----------------------------
-- Checks structure for table SUBJECT_INFO
-- ----------------------------
ALTER TABLE "ORACLE"."SUBJECT_INFO" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SUBJECT_INFO" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SUBJECT_INFO" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SUBJECT_INFO" ADD CHECK ("NAME" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SUBJECT_INFO
-- ----------------------------
ALTER TABLE "ORACLE"."SUBJECT_INFO" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table SYS_DICT
-- ----------------------------

-- ----------------------------
-- Triggers structure for table SYS_DICT
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."sys_dict_TRI" BEFORE INSERT ON "ORACLE"."SYS_DICT" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;
-- ----------------------------
-- Checks structure for table SYS_DICT
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_DICT" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_DICT" ADD CHECK ("VALUE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_DICT" ADD CHECK ("LABEL" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_DICT" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_DICT" ADD CHECK ("VALUE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_DICT" ADD CHECK ("LABEL" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_DICT
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_DICT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table SYS_LOG
-- ----------------------------

-- ----------------------------
-- Triggers structure for table SYS_LOG
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"." SYS_LOG_TRI" BEFORE INSERT ON "ORACLE"."SYS_LOG" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;CREATE OR REPLACE TRIGGER "ORACLE"."SYS_LOG_TRI" BEFORE INSERT ON "ORACLE"."SYS_LOG" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT catalog_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;

-- ----------------------------
-- Checks structure for table SYS_LOG
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_LOG" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_LOG" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_LOG
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_LOG" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table SYS_MDICT
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_MDICT
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("PARENT_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("PARENT_IDS" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("SORT" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("UPDATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("UPDATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("DEL_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("PARENT_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("PARENT_IDS" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("SORT" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("UPDATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("UPDATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MDICT" ADD CHECK ("DEL_FLAG" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_MDICT
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_MDICT" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table SYS_MENU
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_MENU
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("PARENT_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("PARENT_IDS" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("SORT" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("IS_SHOW" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("UPDATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("UPDATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("DEL_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("PARENT_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("PARENT_IDS" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("SORT" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("IS_SHOW" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("UPDATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("UPDATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_MENU" ADD CHECK ("DEL_FLAG" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_MENU
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_MENU" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table SYS_OFFICE
-- ----------------------------

-- ----------------------------
-- Triggers structure for table SYS_OFFICE
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."SYS_OFFICE_TRI" BEFORE INSERT ON "ORACLE"."SYS_OFFICE" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT SYS_OFFICE_SEQ.NEXTVAL INTO :NEW.ID FROM DUAL;
END;

-- ----------------------------
-- Checks structure for table SYS_OFFICE
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_OFFICE" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_OFFICE" ADD CHECK ("PARENT_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_OFFICE" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_OFFICE" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_OFFICE" ADD CHECK ("PARENT_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_OFFICE" ADD CHECK ("NAME" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_OFFICE
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_OFFICE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Indexes structure for table SYS_ROLE
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_ROLE
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("UPDATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("UPDATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("DEL_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("UPDATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("UPDATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE" ADD CHECK ("DEL_FLAG" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_ROLE
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_ROLE" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table SYS_ROLE_MENU
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_ROLE_MENU" ADD CHECK ("ROLE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE_MENU" ADD CHECK ("ROLE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE_MENU" ADD CHECK ("MENU_ID" IS NOT NULL);

-- ----------------------------
-- Checks structure for table SYS_ROLE_OFFICE
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_ROLE_OFFICE" ADD CHECK ("ROLE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE_OFFICE" ADD CHECK ("OFFICE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE_OFFICE" ADD CHECK ("ROLE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_ROLE_OFFICE" ADD CHECK ("OFFICE_ID" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table SYS_USER
-- ----------------------------

-- ----------------------------
-- Triggers structure for table SYS_USER
-- ----------------------------
CREATE OR REPLACE TRIGGER "ORACLE"."SYS_USER_TRI" BEFORE INSERT ON "ORACLE"."SYS_USER" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW ENABLE
BEGIN
   SELECT sys_user_seq.NEXTVAL INTO :NEW.ID FROM DUAL;
END;

-- ----------------------------
-- Checks structure for table SYS_USER
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("OFFICE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("LOGIN_NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("PASSWORD" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("DEL_FLAG" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("COMPANY_ID" IS NOT NULL) DISABLE;
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("OFFICE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("LOGIN_NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("PASSWORD" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("CREATE_BY" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("CREATE_DATE" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("UPDATE_BY" IS NOT NULL) DISABLE;
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("UPDATE_DATE" IS NOT NULL) DISABLE;
ALTER TABLE "ORACLE"."SYS_USER" ADD CHECK ("DEL_FLAG" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_USER
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_USER" ADD PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table SYS_USER_ROLE
-- ----------------------------
ALTER TABLE "ORACLE"."SYS_USER_ROLE" ADD CHECK ("USER_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER_ROLE" ADD CHECK ("ROLE_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER_ROLE" ADD CHECK ("USER_ID" IS NOT NULL);
ALTER TABLE "ORACLE"."SYS_USER_ROLE" ADD CHECK ("ROLE_ID" IS NOT NULL);

-- ----------------------------
-- Checks structure for table user_a
-- ----------------------------
ALTER TABLE "ORACLE"."user_a" ADD CHECK ("id" IS NOT NULL);

-- ----------------------------
-- Checks structure for table user_b
-- ----------------------------
ALTER TABLE "ORACLE"."user_b" ADD CHECK ("id" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table USER_B
-- ----------------------------
CREATE INDEX "ORACLE"."IDX_USER_B_LOOKUP"
ON "ORACLE"."USER_B" ("UID" ASC)
LOGGING
VISIBLE;
